.run_on_merge_request:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_COMMIT_TITLE =~ /chore\(version\)\:.*/'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: always
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      when: always

.run_on_main:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_COMMIT_TITLE =~ /chore\(version\)\:.*/'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      when: on_success

stages:
  - prechecks
  - checks
  - release
  - post-release

formatting & clippy:
  image: archlinux:latest
  stage: prechecks
  extends: [.run_on_merge_request]
  before_script:
    - ./ci/setup.sh
  script:
    - source $HOME/.cargo/env
    - cargo fmt --all -- --color=always --check
    - cargo clippy --no-deps -- -Dwarnings

outdated & audit:
  image: archlinux:latest
  allow_failure: true
  stage: prechecks
  extends: [.run_on_merge_request]
  before_script:
    - ./ci/setup.sh
    - source $HOME/.cargo/env
    - mkdir -p .audit
    - curl -L -o .audit/audit.tar.gz "https://github.com/rustsec/rustsec/releases/download/cargo-audit%2Fv0.17.6/cargo-audit-x86_64-unknown-linux-musl-v0.17.6.tgz"
    - tar xvzf .audit/audit.tar.gz --directory .audit/
    - export PATH="$CI_PROJECT_DIR/.audit:$PATH"
    - mkdir -p .outdated
    - curl -L -o .outdated/outdated.tar.gz "https://github.com/kbknapp/cargo-outdated/releases/download/v0.13.1/cargo-outdated-0.13.1-x86_64-unknown-linux-musl.tar.gz"
    - tar xvzf .outdated/outdated.tar.gz --directory .outdated/
    - export PATH="$CI_PROJECT_DIR/.outdated:$PATH"
  script:
    - source $HOME/.cargo/env
    - cargo outdated -R
    - cargo audit
    - cargo outdated -R --exit-code 1

build and test x86-64:
  image: archlinux:latest
  stage: checks
  extends: [.run_on_merge_request]
  before_script:
    - ./ci/setup.sh
  script:
    - source $HOME/.cargo/env
    - cargo test --release -- --test-threads 1

release:
  image: archlinux:latest
  stage: release
  extends: [.run_on_main]
  before_script:
    - ./ci/setup.sh
    - source $HOME/.cargo/env
    - cargo install cargo-bump
  script:
    - source $HOME/.cargo/env
    - git remote set-url origin https://${GITLAB_USERNAME}:${GITLAB_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git
    - git config --global user.email "gitlab ci"
    - git config --global user.name "gitlab ci"
    - cog bump --auto || cog bump --patch
    - TAG_VERSION=$(grep version Cargo.toml | head -1 | sed 's/version = "//g' | sed 's/"//g')
    - git push --atomic origin v$TAG_VERSION HEAD:$CI_COMMIT_BRANCH
    - cargo publish --token $CRATES_IO_TOKEN

upload_released_binary:
  image: archlinux:latest
  stage: post-release
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /chore\(version\):.*/
  # stage: prechecks
  before_script:
    - ./ci/setup.sh
  script:
    - source $HOME/.cargo/env
    - cargo build --release
    - TAG_VERSION=$(grep version Cargo.toml | head -1 | sed 's/version = "//g' | sed 's/"//g')
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file target/release/${CI_PROJECT_NAME} "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/$TAG_VERSION/${CI_PROJECT_NAME}-$TAG_VERSION"'

create_gitlab_release:
  image: registry.gitlab.com/gitlab-org/release-cli:v0.1.0
  stage: post-release
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo ok
  release:
    tag_name: "$CI_COMMIT_TAG"
    name: "cargo-gra ${CI_COMMIT_TAG:1}"
    description: "Release of cargo-gra version ${CI_COMMIT_TAG:1}."
    assets:
      links:
        - name: "binary"
          url: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${CI_COMMIT_TAG:1}/CI_PROJECT_NAME-${CI_COMMIT_TAG:1}.tar.xz"
