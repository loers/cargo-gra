# Example project

You can test cargo-gra here:

1. build cargo gra
```
# in project root
cargo build --release
```

2. Use cargo-gra in example project

```
# in examples/example-project
../../target/release/cargo-gra gra gen
../../target/release/cargo-gra gra flatpak --prepare
../../target/release/cargo-gra gra flatpak --release
../../target/release/cargo-gra gra flatpak
```