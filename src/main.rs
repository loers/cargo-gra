// SPDX-License-Identifier: GPL-3.0-or-later

use clap::{Parser, Subcommand};

use std::env::current_dir;

pub mod descriptor;
pub use descriptor::*;
pub mod build;
pub use build::*;

#[derive(Parser)]
#[clap(name = "cargo-gra")]
#[clap(bin_name = "cargo-gra")]
#[clap(override_usage = "cargo gra <SUBCOMMAND>")]
struct Cargo {
    // take the self name (gra) argument provided by cargo when calling via cargo gra ...
    #[clap(help = "Own subcommand name (gra)")]
    _self: Option<String>,
    #[clap(subcommand)]
    gra: Gra,
}

#[derive(Subcommand)]
#[clap(author, version, about)]
enum Gra {
    #[clap(about = "Cleanup generated files")]
    Clean,
    #[clap(about = "Create the App.toml file")]
    Init,
    #[clap(
        about = "Generate manifests and source files from your Cargo.toml",
        override_usage = "cargo gra generate"
    )]
    Generate,
    #[clap(
        about = "Alias for cargo gra generate",
        override_usage = "cargo gra gen"
    )]
    Gen,
    #[clap(
        name = "flatpak",
        about = "Build a flatpak app locally",
        override_usage = "cargo gra flatpak"
    )]
    Flatpak(Flatpak),
}

#[derive(Parser)]
struct Flatpak {
    #[clap(
        short,
        long,
        help = "Create a release tar.xz of the sources and manifest.yml with according hash and the given url. Provide 'local' to generate files for a local release."
    )]
    release: Option<String>,
    #[clap(
        short,
        long,
        help = "Only prepare the flatpak-temp. May be used to get everything set up and use flatpak-builder on your own."
    )]
    prepare: bool,
    #[clap(
        short,
        long,
        help = "The architecture to build for. The systems architecture will be used if unset. See https://docs.flatpak.org/en/latest/flatpak-builder-command-reference.html."
    )]
    arch: Option<String>,
}

impl From<Flatpak> for FlatpakArgs {
    fn from(f: Flatpak) -> Self {
        Self {
            arch: f.arch,
            prepare_only: f.prepare,
            release: f.release,
        }
    }
}

fn main() {
    let c: Cargo = Cargo::parse();

    let project_dir = current_dir().unwrap();

    match c.gra {
        Gra::Clean => clean(&project_dir),
        Gra::Init => init(&project_dir),
        Gra::Generate => generate(&project_dir),
        Gra::Gen => generate(&project_dir),
        Gra::Flatpak(command) => {
            if let Err(e) = flatpak(&project_dir, command.into()) {
                eprintln!("[gra] Failed to build flatpak: {}.", e);
            }
        }
    }
}
