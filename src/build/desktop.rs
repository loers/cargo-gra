use std::fs::File;

use std::io::Write;
use std::path::PathBuf;

use std;

use std::path::Path;

use crate::ProjectDescriptor;

pub(crate) fn create_desktop_file(
    descriptor: &ProjectDescriptor,
    path: &Path,
) -> std::io::Result<()> {
    let template = include_str!("../../data/app.template.desktop");

    let app_desc = &descriptor.app;

    let mut path = PathBuf::from(path);
    path.push(format!("{}.desktop", app_desc.id));

    println!("[gra] Generate {:?}", path);
    let mut file = File::create(path)?;

    let generic_name = app_desc
        .generic_name
        .as_ref()
        .unwrap_or(&descriptor.package.name);

    file.write_all(
        template
            .replace("{id}", &app_desc.id)
            .replace(
                "{name}",
                descriptor
                    .app
                    .name
                    .as_ref()
                    .unwrap_or(&descriptor.package.name),
            )
            .replace("{generic_name}", generic_name)
            .replace("{summary}", &app_desc.summary)
            .replace("{categories}", &app_desc.categories.join(";"))
            .replace(
                "{mimetype}",
                app_desc.mimetype.as_ref().unwrap_or(&"".into()),
            )
            .replace(
                "{form_factor}",
                &app_desc
                    .requires
                    .iter()
                    .filter_map(|r| match r {
                        crate::Recommend::Display(d) => Some(d),
                        crate::Recommend::DisplayLength(d) => Some(d),
                        crate::Recommend::Control(_) => None,
                    })
                    .filter_map(|d| match d.as_str() {
                        "small" => Some("Mobile"),
                        ">360" => Some("Mobile"),
                        "large" => Some("Workstation"),
                        _ => None,
                    })
                    .collect::<Vec<&str>>()
                    .join(";"),
            )
            .as_bytes(),
    )?;
    Ok(())
}
