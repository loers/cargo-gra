// SPDX-License-Identifier: GPL-3.0-or-later

use std::{fs::File, io::Write, path::Path};

use crate::ProjectDescriptor;

pub fn build_makefile(_: &ProjectDescriptor, gra_gen_dir: &Path) {
    let template = include_str!("../../data/Makefile");
    let file_path = gra_gen_dir.join("Makefile");
    println!("[gra] Generate {:?}", file_path);
    let mut file = File::create(file_path).expect("Could not create Makefile");
    file.write_all(template.as_bytes())
        .expect("Could not write to Makefile");
}
