#!/bin/bash
docker build -t registry.gitlab.com/floers/cargo-gra/flatpak:44 -f ci/flatpak44.dockerfile .
# docker build -t registry.gitlab.com/floers/cargo-gra/public-x86-64:main -f ci/public-x86-64.dockerfile .
# docker build -t registry.gitlab.com/floers/cargo-gra/public-flatpak:main -f ci/public-flatpak.dockerfile .
# docker push registry.gitlab.com/floers/cargo-gra/public-x86-64:main
# docker push registry.gitlab.com/floers/cargo-gra/public-flatpak:main
docker push registry.gitlab.com/floers/cargo-gra/flatpak:44