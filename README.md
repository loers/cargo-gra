# cargo-gra

[![pipeline status](https://gitlab.com/floers/cargo-gra/badges/main/pipeline.svg)](https://gitlab.com/floers/cargo-gra/-/commits/main)
[![API](https://docs.rs/cargo-gra/badge.svg)](https://docs.rs/cargo-gra)
[![crates.io](https://img.shields.io/crates/v/cargo-gra.svg)](https://crates.io/crates/cargo-gra)

![icon.svg"](icon.svg)

cargo-gra is the CLI for [gtk-rust-app](https://crates.io/crates/gtk-rust-app).

## Usage

1. Create a project with `gtk-rust-app` (See [gtk-rust-app](https://gitlab.com/floers/gtk-rust-app))
2. Generate build files
    ```
    cargo gra generate
    or
    cargo gra gen
    ```
3. Build
    1. Either build a flatpak app in `target/myapp.flatpak` file
        ```
        cargo gra flatpak
        ```
    2. Or build a binary as usual
        ```
        cargo build --release
        ```

See `cargo gra --help` for further information.