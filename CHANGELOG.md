# Changelog
All notable changes to this project will be documented in this file. See [conventional commits](https://www.conventionalcommits.org/) for commit guidelines.

- - -
## v0.6.2 - 2024-11-10
#### Bug Fixes
- Fix wrong appdata manifest generation for display_length - (e03e319) - Florian Loers

- - -

## v0.6.1 - 2024-11-09
#### Bug Fixes
- Allow to set display_length in app manifest - (faf365c) - Florian Loers
#### Continuous Integration
- Automate gitlab release creation - (0894909) - Florian Loers
#### Documentation
- Add manifest documentation - (6e2f962) - Florian Loers

- - -

## v0.6.0 - 2023-04-23
#### Features
- Initialize project via gra init - (e4af13e) - Florian Loers

- - -

## v0.5.0 - 2022-12-29
#### Bug Fixes
- Update project links after move - (ef09f93) - Florian Loers
#### Features
- Allow to specify app name in App.toml - (3c83b32) - Florian Loers
- Specify required dispay sizes via App.toml - (168c5e5) - Florian Loers

- - -

## v0.4.2 - 2022-12-18
#### Bug Fixes
- Remove typo in logs - (3e613b5) - Florian Loers

- - -

## v0.4.1 - 2022-10-30
#### Bug Fixes
- Update project links after move - (5db2aa6) - Florian Loers

- - -

## v0.4.0 - 2022-10-02
#### Features
- Allow to set app mimetype - (04a97c3) - Florian Loers
- Move gra data from Cargo.toml to App.toml - (7a423b4) - Florian Loers
- - -

## v0.3.1 - 2022-06-25
#### Bug Fixes
- Generate `X-Purism-FormFactor` value for desktop files - (74b6ef8) - Florian Loers
- - -

## v0.3.0 - 2022-05-15
#### Features
- Support installing apps as system apps via generated makefile - (24bd449) - Florian Loers
- - -

## v0.2.11 - 2022-05-14
#### Continuous Integration
- Fail on clippy warnings - (1dc3ff7) - Florian Loers
- - -

## v0.2.10 - 2022-05-14
#### Bug Fixes
- Icons now support themeing - (bf59875) - Florian Loers
- - -

## v0.2.9 - 2022-04-01
#### Miscellaneous Chores
- Make clap optional for usage as library - (1f79db1) - Florian Loers
- - -

## v0.2.8 - 2022-03-29
#### Bug Fixes
- Fix wrong appdata.xml release tag generation - (7236e21) - Florian Loers
- - -

## v0.2.7 - 2022-03-29
#### Bug Fixes
- Generate empty content_rating - (9c2d332) - Florian Loers
- Change cli name from 'cargo' to 'cargo-gra' - (4d2a9fa) - Florian Loers
- - -

## v0.2.6 - 2022-03-27
#### Bug Fixes
- Fix wrongs path generation for gettext - (450a160) - Florian Loers
- - -

## v0.2.5 - 2022-03-26
#### Miscellaneous Chores
- Add public flatpak build image - (6555570) - Florian Loers
- - -

## v0.2.4 - 2022-03-26
#### Miscellaneous Chores
- Move build tooling from gtk-rust-app to cargo-gra - (e2b7690) - Florian Loers
- - -

## v0.2.3 - 2022-03-20
#### Miscellaneous Chores
- Bump gtk-rust-app version - (dd16d14) - Florian Loers
- - -

## v0.2.2 - 2022-02-18
#### Miscellaneous Chores
- Bump gtk-rust-app version - (f6f6718) - Florian Loers
- - -

## v0.2.1 - 2022-02-13
#### Continuous Integration
- Run cargo clippy - (8576eee) - Florian Loers
- - -

## v0.2.0 - 2022-01-24
#### Features
- Add `gra flatpak --release` command - (b62a59e) - Florian Loers
- - -

## v0.1.4 - 2022-01-20
#### Documentation
- Print flatpak build command with arch argument - (84c57df) - Florian Loers
- - -

## v0.1.3 - 2022-01-20
#### Bug Fixes
- Fix naming of uninstall-gsettings subcommand - (931efd4) - Florian Loers
- - -

## v0.1.2 - 2022-01-19
#### Bug Fixes
- Flatpak dev build uses correct manifest - (c1ee699) - Florian Loers
- - -

## v0.1.1 - 2022-01-18
#### Miscellaneous Chores
- Use latest gtk-rust-app version - (c92a6b3) - Florian Loers
- - -

## v0.1.0 - 2022-01-18
#### Features
- Log root permission requirement in install-gsettings subcommand - (1be21ea) - Florian Loers
- Add prepare flag to cargo gra flatpak - (881800b) - Florian Loers
#### Miscellaneous Chores
- Update gtk-rust-app dependency version - (7745790) - Florian Loers
- - -

## v0.0.4 - 2022-01-16
#### Continuous Integration
- Fix ci pipeline - (650acd3) - Florian Loers
- - -

## v0.0.3 - 2022-01-10
#### Continuous Integration
- Prevent CI trigger after release - (060b6a8) - Florian Loers
- - -

## v0.0.2 - 2022-01-10
#### Continuous Integration
- Fix git push in release job - (3bc3869) - Florian Loers
- Enable formatting and test stage - (5d52e3a) - Florian Loers
#### Miscellaneous Chores
- Update Cargo.lock - (a1ecfb7) - Florian Loers
- - -

Changelog generated by [cocogitto](https://github.com/cocogitto/cocogitto).