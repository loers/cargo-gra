use std::{
    env::temp_dir,
    fs::create_dir_all,
    path::{Path, PathBuf},
    process::Command,
};

use fs_extra::dir::CopyOptions;
use gra::FlatpakArgs;

#[ignore]
#[test]
fn _test_build_minimal() {
    let test_project = PathBuf::new().join("examples/minimal");
    let target_dir = test_project.join("target");
    assert!(test_project.exists());

    Command::new("rm")
        .arg("-rf")
        .arg(test_project.join("target"))
        .output()
        .unwrap();
    assert!(!test_project.join("target").exists());

    gra::generate(&test_project);

    gra::flatpak(
        &test_project,
        FlatpakArgs {
            release: None,
            arch: None,
            prepare_only: false,
        },
    )
    .expect("Could not build flatpak");

    assert!(target_dir.join("minimal.flatpak").exists());

    println!("\n\nTesting release\n\n");

    gra::flatpak(
        &test_project,
        FlatpakArgs {
            release: Some("local".into()),
            arch: None,
            prepare_only: false,
        },
    )
    .expect("Could not prepare flatpak");

    let mut options = CopyOptions::new();
    options.overwrite = true;
    options.copy_inside = true;
    let test_dir = temp_dir().join("gtk-rust-app-prod-flatpak-test");
    create_dir_all(&test_dir).ok();
    std::fs::copy(
        target_dir.join("minimal.tar.xz"),
        test_dir.join("minimal.tar.xz"),
    )
    .unwrap();
    std::fs::copy(
        target_dir.join("org.example.Minimal.yml"),
        test_dir.join("org.example.Minimal.yml"),
    )
    .unwrap();
    run_flatpak_builder("org.example.Minimal", &test_dir);
    run_flatpak_bundle("org.example.Minimal", &test_dir);
    assert!(test_dir.join("org.example.Minimal.flatpak").exists());
}

#[ignore]
#[test]
fn test_build_complete_example_with_release() {
    let test_project = PathBuf::new()
        .join("examples/complete")
        .canonicalize()
        .unwrap();
    let target_dir = test_project.join("target");
    let flatpak_temp = target_dir.join("flatpak-temp");
    assert!(test_project.exists());

    Command::new("rm")
        .arg("-rf")
        .arg(test_project.join("target"))
        .output()
        .unwrap();
    assert!(!test_project.join("target").exists());

    gra::generate(&test_project);
    assert_gra_gen_dir("org.example.Complete", "complete", &test_project);

    gra::flatpak(
        &test_project,
        FlatpakArgs {
            release: None,
            arch: None,
            prepare_only: true,
        },
    )
    .expect("Could not prepare flatpak");

    assert!(flatpak_temp.exists());
    assert!(flatpak_temp.join(".cargo/config.toml").exists());
    assert!(flatpak_temp.join("target/vendor").exists());
    assert!(flatpak_temp.join("target/vendor/gtk-rust-app").exists());
    assert!(flatpak_temp.join("po").exists());
    assert!(flatpak_temp.join("src").exists());
    assert!(flatpak_temp.join("Cargo.toml").exists());
}

fn run_flatpak_builder(id: &str, current_dir: &PathBuf) {
    let mut build_task = Command::new("flatpak-builder")
        .current_dir(current_dir)
        .arg("--repo=repo")
        .arg("--state-dir=state")
        .arg("--force-clean")
        .arg("host")
        .arg(format!("{}.yml", id))
        .spawn()
        .unwrap();
    build_task.wait().unwrap();
}

fn run_flatpak_bundle(id: &str, current_dir: &PathBuf) {
    let mut bundle_task = Command::new("flatpak")
        .current_dir(current_dir)
        .arg("build-bundle")
        .arg("repo")
        .arg(format!("{}.flatpak", id))
        .arg(id)
        .spawn()
        .unwrap();
    bundle_task.wait().unwrap();
}

fn assert_gra_gen_dir(id: &str, name: &str, project_dir: &Path) {
    let gra_gen_dir = project_dir.join("target/gra-gen");

    assert!(gra_gen_dir.join("assets/resources.gresource.xml").exists());

    assert!(gra_gen_dir.join(format!("data/{}.64.png", id)).exists());
    assert!(gra_gen_dir.join(format!("data/{}.128.png", id)).exists());
    assert!(gra_gen_dir
        .join(format!("data/{}.appdata.xml", id))
        .exists());
    assert!(gra_gen_dir.join(format!("data/{}.desktop", id)).exists());
    assert!(gra_gen_dir.join(format!("data/{}.dev.yml", id)).exists());
    assert!(gra_gen_dir.join(format!("data/{}.svg", id)).exists());
    assert!(gra_gen_dir.join(format!("data/{}.yml", id)).exists());

    assert!(gra_gen_dir
        .join(format!("locale/de/LC_MESSAGES/{}.mo", name))
        .exists());

    assert!(gra_gen_dir.join("po/de.pot").exists());
    assert!(gra_gen_dir.join("po/POTFILES").exists());

    assert!(gra_gen_dir.join("actions.rs").exists());
    assert!(gra_gen_dir.join("compiled.gresource").exists());
    assert!(gra_gen_dir.join("Makefile").exists());
    assert!(gra_gen_dir.join(format!("{}.gschema.xml", id)).exists());
}
